import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { makeStyles } from "@mui/styles";
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "./styles/index";
import { getTableValues } from "./api/method";
import ElectronicFormContext from "./Context";
import { sectionA } from "./routes/sectionA";
import { sectionB } from "./routes/sectionB";
import { Navbar } from "./components/reusable/index";
import MyProfile from "./components/pages/profile/MyProfile";
import EditProfile from "./components/pages/profile/EditProfile";
import ChangePassword from "./components/pages/profile/ChangePassword";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 62,
    padding: 20,
  },
}));

function Root() {
  const { root } = useStyles();
  const [storedTableData, setStoredTableData] = React.useState(null);

  const fetchData = async () => {
    const { data } = await getTableValues();
    data.map((item) => {
      return setStoredTableData(item.formData);
    });
  };

  React.useEffect(() => {
    fetchData();
  }, []);

  return (
    <ElectronicFormContext.Provider value={storedTableData}>
      <ThemeProvider theme={theme}>
        <Router>
          <Navbar />
          <div className={root}>
            <Routes>
              <Route path={"/"} element={""} />

              <Route path={"/profile"} element={<MyProfile />} />
              <Route path={"/editProfile"} element={<EditProfile />} />
              <Route path={"/changePassword"} element={<ChangePassword />} />

              {sectionA.map((secA, index) => (
                <Route path={secA.path} element={secA.component} key={index} />
              ))}
              {sectionB.map((secB, index) => (
                <Route path={secB.path} element={secB.component} key={index} />
              ))}
            </Routes>
          </div>
        </Router>
      </ThemeProvider>
    </ElectronicFormContext.Provider>
  );
}

export default Root;
