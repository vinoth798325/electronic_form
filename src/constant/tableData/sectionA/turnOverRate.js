import { CustomTextField } from "../../../components/reusable/index";
import { EditableCell } from "../../../components/utils/EditableCell";

export const turnOverRateColumns = [
  {
    Header: " ",
    accessor: "sNo",
  },

  {
    id: 1,
    Header: (props) => (
      <span>
        FY <CustomTextField headerField="headerField" type="number" /> (Turnover
        rate in current FY)
      </span>
    ),
    columns: [
      {
        Header: "Male",

        accessor: "currentMale",
        type: "number",
        Cell: EditableCell,
      },
      {
        Header: "Female",
        accessor: "currentFemale",
        type: "number",
        Cell: EditableCell,
      },
    ],
  },
  {
    id: 2,
    Header: (props) => (
      <span>
        FY <CustomTextField headerField="headerField" type="number" /> (Turnover
        rate in previous FY)
      </span>
    ),
    columns: [
      {
        Header: "Male",
        accessor: "preMale",
        type: "number",
        Cell: EditableCell,
      },
      {
        Header: "Female",
        accessor: "preFemale",
        type: "number",
        Cell: EditableCell,
      },
    ],
  },
  {
    id: 3,
    Header: (props) => (
      <span>
        FY <CustomTextField headerField="headerField" type="number" /> (Turnover
        rate in the year prior to the previous FY)
      </span>
    ),
    columns: [
      {
        Header: "Male",
        accessor: "male",
        type: "number",
        Cell: EditableCell,
      },
      {
        Header: "Female",
        accessor: "female",
        type: "number",
        Cell: EditableCell,
      },
    ],
  },
];

export const turnOverRateColumnsellData = [
  {
    id: "1",
    sNo: "Permanent Employees",
    currentMale: "",
    currentFemale: "",
    preMale: "",
    preFemale: "",
    male: "",
    female: "",
  },
  {
    id: "2",
    sNo: "Permanent Workers",
    currentMale: "",
    currentFemale: "",
    preMale: "",
    preFemale: "",
    male: "",
    female: "",
  },
];
