import { EditableCell } from "../../../components/utils/EditableCell";

export const transparencyAndDisclosureColumns = [
  {
    Header: "Stakeholder group from whom complaint is received ",
    accessor: "stakeholder",
  },
  {
    Header:
      "Grievance Redressal Mechanism in Place (Yes/No) (If Yes, then provide web-link for grievance redress policy) ",
    accessor: "redressalMechanism",
    Cell: EditableCell,
  },

  {
    Header: "FY _____ Current Financial Year",
    columns: [
      {
        Header: "Number of complaints filed during the year",
        accessor: "complaintsFiled",
        type: "number",
        Cell: EditableCell,
      },
      {
        Header: "Number of complaints pending resolution at close of the year",
        accessor: "complaintsPending",
        type: "number",
        Cell: EditableCell,
      },
      {
        Header: "Remarks",
        accessor: "remarks",
        Cell: EditableCell,
      },
    ],
  },
  {
    Header: "FY _____ Previous Financial Year",
    columns: [
      {
        Header: "Number of complaints filed during the year",
        accessor: "complaintsFiledPrevious",
        type: "number",
        Cell: EditableCell,
      },
      {
        Header: "Number of complaints pending resolution at close of the year",
        accessor: "complaintsPendingPrevious",
        type: "number",
        Cell: EditableCell,
      },
      {
        Header: "Remarks",
        accessor: "remarksPrevious",
        Cell: EditableCell,
      },
    ],
  },
];

export const transparencyAndDisclosureCellData = [
  {
    id: "1",
    stakeholder: "Communities",
    redressalMechanism: "",
    complaintsFiled: "",
    complaintsPending: "",
    remarks: "",
    complaintsFiledPrevious: "",
    complaintsPendingPrevious: "",
    remarksPrevious: "",
  },
  {
    id: "2",
    stakeholder: "Investors (other than shareholders)",
    redressalMechanism: "",
    complaintsFiled: "",
    complaintsPending: "",
    remarks: "",
    complaintsFiledPrevious: "",
    complaintsPendingPrevious: "",
    remarksPrevious: "",
  },
  {
    id: "3",
    stakeholder: "Shareholders",
    redressalMechanism: "",
    complaintsFiled: "",
    complaintsPending: "",
    remarks: "",
    complaintsFiledPrevious: "",
    complaintsPendingPrevious: "",
    remarksPrevious: "",
  },
  {
    id: "4",
    stakeholder: "Employees and workers",
    redressalMechanism: "",
    complaintsFiled: "",
    complaintsPending: "",
    remarks: "",
    complaintsFiledPrevious: "",
    complaintsPendingPrevious: "",
    remarksPrevious: "",
  },
  {
    id: "5",
    stakeholder: "Customers",
    redressalMechanism: "",
    complaintsFiled: "",
    complaintsPending: "",
    remarks: "",
    complaintsFiledPrevious: "",
    complaintsPendingPrevious: "",
    remarksPrevious: "",
  },
  {
    id: "6",
    stakeholder: "Value Chain Partners",
    redressalMechanism: "",
    complaintsFiled: "",
    complaintsPending: "",
    remarks: "",
    complaintsFiledPrevious: "",
    complaintsPendingPrevious: "",
    remarksPrevious: "",
  },
  {
    id: "7",
    stakeholder: "Other (please specify)",
    redressalMechanism: "",
    complaintsFiled: "",
    complaintsPending: "",
    remarks: "",
    complaintsFiledPrevious: "",
    complaintsPendingPrevious: "",
    remarksPrevious: "",
  },
];
