import { toggleButtonSecA, toggleButtonSecB, principles } from "./sidebar";

export const accordionData = [
  {
    id: "1",
    name: "SECTION A",
    buttons: toggleButtonSecA,
    key: "panel1",
  },
  {
    id: "2",
    name: "SECTION B",
    buttons: toggleButtonSecB,
    key: "panel2",
  },
  {
    id: "3",
    name: "SECTION C",
    buttons: principles,
    key: "panel3",
  },
];
