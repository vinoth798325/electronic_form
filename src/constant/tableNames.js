export const SECTIONA_BUSINESSACTIVITIES = "sectionA/businessActivities";
export const SECTIONA_DIFFERENTABLED = "sectionA/differentAbled";
export const SECTIONA_EMPLOYEESANDWORKERS = "sectionA/employeesAndWorkers";
export const SECTIONA_JOINTVENTURES = "sectionA/jointVentures";
export const SECTIONA_NUMBEROFLOCATIONS = "sectionA/numberOfLocations";
export const SECTIONA_OVERVIEWENTITY = "sectionA/overviewEntity";
export const SECTIONA_PARTICIPATIONOFWOMEN = "sectionA/participationOfWomen";
export const SECTIONA_PRODUCTSERVICE = "sectionA/productService";
export const SECTIOA_TRANSPARENCYANDDISCLOSURE =
  "sectionA/transparencyAndDisclosure";
export const SECTIONA_TURNOVERRATE = "sectionA/turnOverRate";
export const SECTIONA_LISTEDENTITY = "sectionA/listedEntity";
export const SECTIONA_MARKETSSERVEDENTITY = "sectionA/marketsServedEntity";
export const SECTIONA_CSRDETAILS = "sectionA/CSRdetails";
