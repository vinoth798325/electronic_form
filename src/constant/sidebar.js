export const toggleButtonSecA = [
  {
    id: 1,
    name: "Listed Entity",
    value: "listedEntity",
    path: "/sectionA/listedEntity",
    tooltipContent: "Details of the listed entity",
  },
  {
    id: 2,
    name: "Business Activities",
    value: "businessActivities",
    path: "/sectionA/businessActivities",
    tooltipContent:
      "Details of business activities (accounting for 90% of the turnover):",
  },
  {
    id: 3,
    name: "Product/Services by entity",
    value: "productServicesByEntity",
    path: "/sectionA/productServicesByEntity",
    tooltipContent:
      "Products/Services sold by the entity (accounting for 90% of the entity’s Turnover):",
  },
  {
    id: 4,
    name: "Number of locations",
    value: "numberOfLocations",
    path: "/sectionA/numberOfLocations",
    tooltipContent:
      "Number of locations where plants and/or operations/offices of the entity are situated:",
  },
  {
    id: 5,
    name: "Markets served by entity",
    value: "marketsServedEntity",
    path: "/sectionA/marketsServedEntity",
    tooltipContent: "Markets served by the entity:",
  },
  {
    id: 6,
    name: "Employees and Workers",
    value: "employeesAndWorkers",
    path: "/sectionA/employeesAndWorkers",
    tooltipContent: " Employees and workers (including differently abled):",
  },
  {
    id: 7,
    name: "Differently abled Employees and Workers",
    value: "differentlyAbledEmployeesAndWorkers",
    path: "/sectionA/differentlyAbledEmployeesAndWorkers",
    tooltipContent: "Differently abled Employees and workers:",
  },
  {
    id: 8,
    name: "Participation of Women",
    value: "participationOfWomen",
    path: "/sectionA/participationOfWomen",
    tooltipContent: "Participation/Inclusion/Representation of women",
  },
  {
    id: 9,
    name: "Turnover Rate",
    value: "turnoverRate",
    path: "/sectionA/turnoverRate",
    tooltipContent: "Turnover rate for permanent employees and workers",
  },
  {
    id: 10,
    name: "Associate companies/ Joint ventures",
    value: "jointVentures",
    path: "/sectionA/jointVentures",
    tooltipContent:
      "Names of holding / subsidiary / associate companies / joint ventures",
  },
  {
    id: 11,
    name: "CSR Details",
    value: "csrDetails",
    path: "/sectionA/csrDetails",
    tooltipContent: "CSR Details",
  },
  {
    id: 12,
    name: "Transparency and Disclosures Compliances",
    value: "transparencyAndDisclosures",
    path: "/sectionA/transparencyAndDisclosures",
    tooltipContent:
      "Complaints/Grievances on any of the principles (Principles 1 to 9) under the National Guidelines on Responsible Business Conduct:",
  },
  {
    id: 13,
    name: "Overview of the entity’s",
    value: "overviewEntity",
    path: "/sectionA/overviewEntity",
    tooltipContent:
      "Overview of the entity’s material responsible business conduct issues",
  },
];

export const toggleButtonSecB = [
  {
    id: 1,
    name: "Disclosure Questions",
    tooltipContent:
      "This section is aimed at helping businesses demonstrate the structures, policies and processes put in place towards adopting the NGRBC Principles and Core Elements.",
    value: "disclosureQuestions",
    path: "sectionB/disclosureQuestions",
  },
  {
    id: 2,
    name: "Review of NGRBCs",
    tooltipContent: "Details of Review of NGRBCs by the Company:",
    value: "reviewOfNGRBCs",
    path: "sectionB/reviewOfNGRBCs",
  },
  {
    id: 3,
    name: "Assessment / Evaluation",
    tooltipContent:
      "Has the entity carried out independent  assessment/ evaluation of the working of  its policies by an external agency?",
    value: "assessmentEvaluation",
    path: "sectionB/assessmentEvaluation",
  },
  {
    id: 4,
    name: "Questions",
    tooltipContent:
      "If answer to question (1) above is “No” i.e. not all Principles are covered by a policy, reasons to be stated:",

    value: "questions",
    path: "sectionB/questions",
  },
];

export const principles = [
  {
    id: "1",
    name: "PRINCIPLE 1",
    key: "panel1",
    buttons: [
      {
        id: 1,
        name: "Essential Indicators",
        value: "essentialIndicators1",
        path: "/essentialIndicators1",
        tooltipContent:
          "Percentage coverage by training and awareness programmes on any of the Principles during the financial year:",
      },
      {
        id: 2,
        name: "Details of Fines / Penalties",
        value: "finesPenalties",
        path: "/finesPenalties",
        tooltipContent:
          "Details of fines / penalties /punishment/ award/ compounding fees/ settlement amount",
      },
      {
        id: 3,
        name: "Case Details",
        value: "caseDetails",
        path: "/caseDetails",
        tooltipContent:
          "Of the instances disclosed in Question 2 above, details of the Appeal/ Revision preferred in cases where monetary or non-monetary action has been appealed.",
      },
      {
        id: 4,
        name: "Bribery / Corruption",
        value: "briberyCorruption",
        path: "/briberyCorruption",
        tooltipContent:
          "Number of Directors/KMPs/employees/workers against whom disciplinary action was taken by any law enforcement agency for the charges of bribery/ corruption:",
      },
      {
        id: 5,
        name: "Conflict of Interest",
        value: "conflictOfInterest",
        path: "/conflictOfInterest",
        tooltipContent:
          "Details of complaints with regard to conflict of interest:",
      },
      {
        id: 6,
        name: "Leadership Indicators",
        value: "leadershipIndicators",
        path: "/leadershipIndicators",
        tooltipContent:
          "Awareness programmes conducted for value chain partners on any of the Principles during the financial year:",
      },
    ],
  },
  {
    id: "2",
    name: "PRINCIPLE 2",
    key: "panel2",

    buttons: [
      {
        id: 1,
        name: "Essential Indicators",
        value: "essentialIndicators2",
        path: "/essentialIndicators2",
        tooltipContent:
          ". Percentage of R&D and capital expenditure (capex) investments in specific technologies to improve the environmental and social impacts of product and processes to total R&D and capex investments made by the entity, respectively.",
      },
      {
        id: 2,
        name: "Life Cycle Perspective / Assessments (LCA)",
        value: "LCA",
        path: "/LCA",
        tooltipContent:
          "Has the entity conducted Life Cycle Perspective / Assessments (LCA) for any of its  products (for manufacturing industry) or for its services (for service industry)? If yes, provide details in the following format?",
      },
      {
        id: 3,
        name: "Description of the risk / concern",
        value: "riskConcern",
        path: "/riskConcern",
        tooltipContent:
          "If there are any significant social or environmental concerns and/or risks arising from  production or disposal of your products / services, as identified in the Life Cycle Perspective / Assessments (LCA) or through any other means, briefly describe the same along-with action taken to mitigate the same.",
      },
      {
        id: 4,
        name: "Percentage of recycled or reused",
        value: "recycledReused",
        path: "/recycledReused",
        tooltipContent:
          "Percentage of recycled or reused input material to total material (by value) used in production (for manufacturing industry) or providing services (for service industry).",
      },
      {
        id: 5,
        name: "Products and packaging reclaimed",
        value: "productsPackaging",
        path: "/productsPackaging",
        tooltipContent:
          "Of the products and packaging reclaimed at end of life of products, amount (in metric tonnes) reused, recycled, and safely disposed, as per the following format:",
      },
      {
        id: 6,
        name: "Percentage of products sold",
        value: "productsSold",
        path: "/productsSold",
        tooltipContent:
          "Reclaimed products and their packaging materials (as percentage of products sold) for each product category",
      },
    ],
  },
  {
    id: "3",
    name: "PRINCIPLE 3",
    key: "panel3",

    buttons: [
      {
        id: 1,
        name: "% of employees covered",
        value: "employeesCovered",
        path: "/employeesCovered",
        tooltipContent: "Details of measures for the well-being of employees:",
      },
      {
        id: 2,
        name: "% of workers covered",
        value: "workersCovered",
        path: "/workersCovered",
        tooltipContent: "Details of measures for the well-being of workers:",
      },
      {
        id: 3,
        name: "Details of retirement benefits",
        value: "retirementBenefits",
        path: "/retirementBenefits",
        tooltipContent:
          "Details of retirement benefits, for Current FY and Previous Financial Year.",
      },
      {
        id: 4,
        name: "Retention rates of parental leave",
        value: "parentalLeave",
        path: "/parentalLeave",
        tooltipContent:
          "Return to work and Retention rates of permanent employees and workers that took parental leave.",
      },
      {
        id: 5,
        name: "Mechanism",
        value: "mechanism",
        path: "/mechanism",
        tooltipContent:
          "Is there a mechanism available to receive and redress grievances for the following categories of employees and worker? If yes, give details of the mechanism in brief.",
      },
      {
        id: 6,
        name: "Membership of employees",
        value: "membershipOfEmployees",
        path: "/membershipOfEmployees",
        tooltipContent:
          "Membership of employees and worker in association(s) or Unions recognised by the listed entity:",
      },
      {
        id: 7,
        name: "Details of training",
        value: "training",
        path: "/training",
        tooltipContent: "Details of training given to employees and workers:",
      },
      {
        id: 8,
        name: "Details of performance",
        value: "performance",
        path: "/performance",
        tooltipContent:
          "Details of performance and career development reviews of employees and worker:",
      },
      {
        id: 9,
        name: "Safety related incidents",
        value: "safety",
        path: "/safety",
        tooltipContent:
          "Details of safety related incidents, in the following format:",
      },
      {
        id: 10,
        name: "Number of Complaints",
        value: "complaints",
        path: "/complaints",
        tooltipContent:
          "Number of Complaints on the following made by employees and workers:",
      },
      {
        id: 11,
        name: "Assessments for the year",
        value: "assessments",
        path: "/assessments",
        tooltipContent: "Assessments for the year:",
      },
      {
        id: 12,
        name: "Affected employees and workers",
        value: "turnoverRate",
        path: "/turnoverRate",
        tooltipContent:
          "Provide the number of employees / workers having suffered high consequence work- related injury / ill-health / fatalities",
      },
      {
        id: 13,
        name: "Details on assessment",
        value: "detailsOnAssessment",
        path: "/detailsOnAssessment",
        tooltipContent: "Turnover rate for permanent employees and workers",
      },
    ],
  },
  {
    id: "4",
    name: "PRINCIPLE 4",
    key: "panel4",

    buttons: [
      {
        id: 1,
        name: "Essential Indicators",
        value: "essentialIndicators4",
        path: "/essentialIndicators4",
        tooltipContent:
          "List stakeholder groups identified as key for your entity and the frequency of  engagement with each stakeholder group.",
      },
    ],
  },
  {
    id: "5",
    name: "PRINCIPLE 5",
    key: "panel5",

    buttons: [
      {
        id: 1,
        name: "Essential Indicators",
        value: "essentialIndicators5",
        path: "/essentialIndicators5",
        tooltipContent:
          "Employees and workers who have been provided training on human rights issues and policy(ies) of the entity, in the following format:",
      },
      {
        id: 2,
        name: "Details of minimum wages",
        value: "minimumWages",
        path: "/minimumWages",
        tooltipContent:
          "Details of minimum wages paid to employees and workers, in the following format:",
      },
      {
        id: 3,
        name: "Details of remuneration/salary/wages",
        value: "remunerationSalary",
        path: "/remunerationSalary",
        tooltipContent:
          "Details of remuneration/salary/wages, in the following format:",
      },
      {
        id: 4,
        name: "Number of Complaints",
        value: "numberOfComplaints",
        path: "/numberOfComplaints",
        tooltipContent:
          "Number of Complaints on the following made by employees and workers:",
      },
      {
        id: 5,
        name: "Assessments for the year",
        value: "assessments5",
        path: "/assessments5",
        tooltipContent: "Assessments for the year:",
      },
      {
        id: 6,
        name: "Details on assessment",
        value: "detailsOnAssessment5",
        path: "/detailsOnAssessment5",
        tooltipContent: "Details on assessment of value chain partners:",
      },
    ],
  },
  {
    id: "6",
    name: "PRINCIPLE 6",
    key: "panel6",

    buttons: [
      {
        id: 1,
        name: "Essential Indicators",
        value: "essentialIndicators6",
        path: "/essentialIndicators6",
        tooltipContent:
          "Details of total energy consumption (in Joules or multiples) and energy intensity, in the following format:",
      },
      {
        id: 2,
        name: "Disclosures related to water",
        value: "disclosuresWater",
        path: "/disclosuresWater",
        tooltipContent:
          "Provide details of the following disclosures related to water, in the following format:",
      },
      {
        id: 3,
        name: "Air emissions",
        value: "airEmissions",
        path: "/airEmissions",
        tooltipContent:
          "Please provide details of air emissions (other than GHG emissions) by the entity, in the following format:",
      },
      {
        id: 4,
        name: "Greenhouse gas emissions",
        value: "greenhouseGas",
        path: "/greenhouseGas",
        tooltipContent:
          "Provide details of greenhouse gas emissions (Scope 1 and Scope 2 emissions) & its intensity, in the following format:",
      },
      {
        id: 5,
        name: "Waste management",
        value: "wasteManagement",
        path: "/wasteManagement",
        tooltipContent:
          "Provide details related to waste management by the entity, in the following format:",
      },
      {
        id: 6,
        name: "Location of operations / offices",
        value: "operationsOffices",
        path: "/operationsOffices",
        tooltipContent:
          "If the entity has operations/offices in/around ecologically sensitive areas (such as national parks, wildlife sanctuaries, biosphere reserves, wetlands, biodiversity hotspots, forests, coastal regulation zones etc.)",
      },
      {
        id: 7,
        name: "Environmental impact assessments",
        value: "environmentalAssessments",
        path: "/environmentalAssessments",
        tooltipContent:
          "Details of environmental impact assessments of projects undertaken by the entity based on applicable laws, in the current financial year:",
      },
      {
        id: 8,
        name: "Prevention and Control of Pollution",
        value: "pollution",
        path: "/pollution",
        tooltipContent:
          "Is the entity compliant with the applicable environmental law/ regulations/ guidelines in India; such as the Water (Prevention and Control of Pollution) Act, Air (Prevention and Control of Pollution)",
      },
      {
        id: 9,
        name: "Energy consumed",
        value: "energyConsumed",
        path: "/energyConsumed",
        tooltipContent:
          "Provide break-up of the total energy consumed (in Joules or multiples) from renewable and non-renewable sources, in the following format:",
      },
      {
        id: 10,
        name: "Water discharged",
        value: "waterDischarged",
        path: "/waterDischarged",
        tooltipContent:
          "Provide the following details related to water discharged:",
      },
      {
        id: 11,
        name: "Water withdrawal, consumption",
        value: "waterWithdrawal",
        path: "/waterWithdrawal",
        tooltipContent:
          "Water withdrawal, consumption and discharge in areas of water stress (in kilolitres):",
      },
      {
        id: 12,
        name: "Emissions & its intensity",
        value: "emissions",
        path: "/emissions",
        tooltipContent:
          "Please provide details of total Scope 3 emissions & its intensity, in the following format:",
      },
      {
        id: 13,
        name: "Emissions / effluent discharge",
        value: "effluentDischarge",
        path: "/effluentDischarge",
        tooltipContent:
          "If the entity has undertaken any specific initiatives or used innovative technology or solutions to improve resource efficiency, or reduce impact due to emissions / effluent discharge / waste generated",
      },
    ],
  },
  {
    id: "7",
    name: "PRINCIPLE 7",
    key: "panel7",

    buttons: [
      {
        id: 1,
        name: "Chambers/ associations",
        value: "chambers",
        path: "/chambers",
        tooltipContent:
          "List the top 10 trade and industry chambers/ associations (determined based on the  total members of such body) the entity is a member of/ affiliated to.",
      },
      {
        id: 2,
        name: "Corrective action taken",
        value: "corrective",
        path: "/corrective",
        tooltipContent:
          "Provide details of corrective action taken or underway on any issues related to anti- competitive conduct by the entity, based on adverse orders from regulatory authorities.",
      },
      {
        id: 3,
        name: "Public policy",
        value: "publicPolicy",
        path: "/publicPolicy",
        tooltipContent:
          "Details of public policy positions advocated by the entity:",
      },
    ],
  },
  {
    id: "8",
    name: "PRINCIPLE 8",
    key: "panel8",

    buttons: [
      {
        id: 1,
        name: "Social Impact Assessments (SIA)",
        value: "SIA",
        path: "/SIA",
        tooltipContent:
          "Details of Social Impact Assessments (SIA) of projects undertaken by the entity based on applicable laws, in the current financial year.",
      },
      {
        id: 2,
        name: "Rehabilitation and Resettlement (R&R)",
        value: "R&R",
        path: "/R&R",
        tooltipContent:
          "Provide information on project(s) for which ongoing Rehabilitation and Resettlement (R&R) is being undertaken by your entity, in the following format:",
      },
      {
        id: 3,
        name: "Percentage of input material",
        value: "inputMaterial",
        path: "/inputMaterial",
        tooltipContent:
          "Percentage of input material (inputs to total inputs by value) sourced from suppliers:",
      },
      {
        id: 4,
        name: "Negative social impacts",
        value: "negativeImpacts",
        path: "/negativeImpacts",
        tooltipContent:
          "Provide details of actions taken to mitigate any negative social impacts identified in the Social Impact Assessments (Reference: Question 1 of Essential Indicators above):",
      },
      {
        id: 5,
        name: "CSR projects",
        value: "CSR",
        path: "/CSR",
        tooltipContent:
          "Provide the following information on CSR projects undertaken by your entity in designated aspirational districts as identified by government bodies",
      },
      {
        id: 6,
        name: "Intellectual Property",
        value: "intellectualProperty",
        path: "/intellectualProperty",
        tooltipContent:
          "Details of the benefits derived and shared from the intellectual properties owned or acquired by your entity (in the current financial year), based on traditional knowledge:",
      },
      {
        id: 7,
        name: "Corrective action taken",
        value: "corrective8",
        path: "/corrective8",
        tooltipContent:
          "Details of corrective actions taken or underway, based on any adverse order in intellectual property related disputes wherein usage of traditional knowledge is involved.",
      },
      {
        id: 8,
        name: "Beneficiaries of CSR Projects",
        value: "beneficiariesCSR",
        path: "/beneficiariesCSR",
        tooltipContent: "Details of beneficiaries of CSR Projects:",
      },
    ],
  },
  {
    id: "9",
    name: "PRINCIPLE 9",
    key: "panel9",

    buttons: [
      {
        id: 1,
        name: "Percentage of turnover",
        value: "turnover",
        path: "/turnover",
        tooltipContent:
          "Turnover of products and/ services as a percentage of turnover from all products/service that carry information about:",
      },
      {
        id: 2,
        name: "Consumer complaints",
        value: "consumerComplaints9",
        path: "/consumerComplaints9",
        tooltipContent:
          "Number of consumer complaints in respect of the following:",
      },
      {
        id: 3,
        name: "Details of instances",
        value: "instances",
        path: "/instances",
        tooltipContent:
          "Details of instances of product recalls on account of safety issues:",
      },
    ],
  },
];
