export const turnOverRateContents = [
  {
    id: "1",
    title: " SECTION A: GENERAL DISCLOSURES",
    subTitle: "IV. Employees",
    aboutTable: "20. Turnover rate for permanent employees and workers",
    subAboutTable: "(Disclose trends for the past 3 years)",
  },
];
