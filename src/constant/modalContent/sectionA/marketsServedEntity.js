export const marketsServedEntityContents = [
  {
    id: "1",
    title: " SECTION A: GENERAL DISCLOSURES",
    subTitle: "III. Operations",
    aboutTable: "17. Markets served by the entity:",
    subAboutTable: "a. Number of locations",
  },
];
