export const numberOfLocationsContents = [
  {
    id: "1",
    title: " SECTION A: GENERAL DISCLOSURES",
    subTitle: "III. Operations",
    aboutTable:
      "16. Number of locations where plants and/or operations/offices of the entity are situated:",
  },
];
