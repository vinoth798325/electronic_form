import axios from "axios";

export const appApi = axios.create({
  baseURL: `http://localhost:4000`,
  responseType: "json",
  headers: {
    Accept: "application/json",
    "content-type": "application/json",
  },
});

appApi.interceptors.request.use(
  (config) => {
    config.headers["authorization"] = `No Authentication`;

    return config;
  },
  (error) => {
    console.error(error);
    return Promise.reject(error);
  }
);
