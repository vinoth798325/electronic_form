import { appApi } from "./config";

const custId = "4b3edcd0-a83d-4b36-8513-b6e339af3807";

const dataFormat = (sectionName, tableData) => {
  return {
    formId: "150c2c13-bbb1-4345-ac73-504e17834dd0",
    section: `${sectionName}`,
    data: tableData,
  };
};

export const getTableValues = () => {
  return appApi.get(`getFormDataByCust/${custId}`);
};

export const createTableValues = (sectionName, tableData) =>
  appApi.post("createFormData", dataFormat(sectionName, tableData));

export const updateTableValues = (sectionName, tableData, id) => {
  return appApi.put(`updateFormData/${id}`, dataFormat(sectionName, tableData));
};
