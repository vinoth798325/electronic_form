import React from "react";
import { makeStyles } from "@mui/styles";
import { useNavigate } from "react-router-dom";
import { Typography, Box, Paper, IconButton, Button } from "@mui/material";
import CameraAltIcon from "@mui/icons-material/CameraAlt";
import TextField from "@mui/material/TextField";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "10px 20px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  header: {
    fontWeight: "900 !important",
    marginLeft: "5px !important",
  },
  headerContainer: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    marginBottom: "20px !important",
  },
  uploadImg: {
    border: "1px dashed #8284ab  !important",
    width: "200px !important",
    height: "100% !important",
    padding: "14px !important",
    color: "#8284ab !important",
  },
  bottomContainer: {
    display: "flex",
    justifyContent: "flex-end",
    padding: 20,
  },
  cancelBtn: {
    backgroundColor: "#c0c1ce !important",
    marginRight: "20px !important",
    color: "white !important",
  },
}));

function EditProfile() {
  const {
    root,
    header,
    headerContainer,
    bottomContainer,
    cancelBtn,
    uploadImg,
  } = useStyles();
  const navigate = useNavigate();

  return (
    <>
      <div className={root}>
        <Box className={headerContainer}>
          <IconButton onClick={() => navigate("/profile")}>
            <ArrowBackIcon />
          </IconButton>
          <Typography variant="h6" className={header}>
            EDIT PROFILE
          </Typography>
        </Box>
        <Box
          component="form"
          sx={{
            width: "70%",
            border: "2px solid #bbc9db",
            borderRadius: "10px",
            padding: "25px",
          }}
          noValidate
          autoComplete="off"
        >
          <Box
            sx={{
              "& > :not(style)": { m: 2, width: "46.6%" },
            }}
          >
            <TextField
              id="outlined-basic"
              label="User Name *"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="User Email *"
              variant="outlined"
            />
          </Box>
          <Box
            sx={{
              "& > :not(style)": { m: 2, width: "46.6%" },
            }}
          >
            <TextField
              id="outlined-basic"
              label="User Mobile *"
              variant="outlined"
            />
            <Button className={uploadImg} startIcon={<CameraAltIcon />}>
              Profile Image
            </Button>
          </Box>
          <Box
            sx={{
              maxWidth: "100%",
              m: 2,
            }}
          >
            <TextField fullWidth label="Address" id="fullWidth" />
          </Box>
          <Box
            sx={{
              "& > :not(style)": { m: 2, width: "46.6%" },
            }}
          >
            <TextField id="outlined-basic" label="Country" variant="outlined" />
            <TextField id="outlined-basic" label="State" variant="outlined" />
          </Box>

          <Box
            sx={{
              "& > :not(style)": { m: 2, width: "29.9%" },
            }}
          >
            <TextField
              id="outlined-basic"
              label="District"
              variant="outlined"
            />
            <TextField id="outlined-basic" label="City" variant="outlined" />
            <TextField
              id="outlined-basic"
              label="Pincode/Zipcode"
              variant="outlined"
            />
          </Box>
          <Box className={bottomContainer}>
            <Button className={cancelBtn}>Cancel</Button>
            <Button variant="contained" color="success">
              Update
            </Button>
          </Box>
        </Box>
      </div>
    </>
  );
}

export default EditProfile;
