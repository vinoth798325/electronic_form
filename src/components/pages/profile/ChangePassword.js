import React from "react";
import { makeStyles } from "@mui/styles";
import { useNavigate } from "react-router-dom";
import { Typography, Box, IconButton, Button } from "@mui/material";
import TextField from "@mui/material/TextField";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "10px 20px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  header: {
    fontWeight: "900 !important",
    marginLeft: "5px !important",
  },
  headerContainer: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    marginBottom: "20px !important",
  },
  bottomContainer: {
    display: "flex",
    justifyContent: "flex-end",
  },
  cancelBtn: {
    // backgroundColor: "#c0c1ce !important",
    marginRight: "20px !important",
    color: "#8e8f93 !important",
    border: "1px solid #8e8f93 !important",
  },
}));

function ChangePassword() {
  const { root, header, headerContainer, bottomContainer, cancelBtn } =
    useStyles();
  const navigate = useNavigate();

  return (
    <>
      <div className={root}>
        <Box className={headerContainer}>
          <IconButton onClick={() => navigate("/profile")}>
            <ArrowBackIcon />
          </IconButton>
          <Typography variant="h6" className={header}>
            CHANGE PASSWORD
          </Typography>
        </Box>
        <Box
          component="form"
          sx={{
            width: "25%",
            border: "2px solid #727172",
            borderRadius: "10px",
            padding: "25px",
            "& > :not(style)": { mt: 2, mb: 2 },
          }}
          noValidate
          autoComplete="off"
        >
          <TextField
            id="outlined-basic"
            fullWidth
            label="Old Password*"
            variant="outlined"
          />
          <TextField
            id="outlined-basic"
            fullWidth
            label="New Password*"
            variant="outlined"
          />
          <TextField
            id="outlined-basic"
            fullWidth
            label="Confirm New Password"
            variant="outlined"
          />
          <Box className={bottomContainer}>
            <Button variant="outlined" className={cancelBtn}>
              Cancel
            </Button>
            <Button variant="contained" color="success">
              Update
            </Button>
          </Box>
        </Box>
      </div>
    </>
  );
}

export default ChangePassword;
