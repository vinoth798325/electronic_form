import React from "react";
import { makeStyles } from "@mui/styles";
import { useNavigate } from "react-router-dom";
import { Typography, Box, IconButton, Button } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import LockIcon from "@mui/icons-material/Lock";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "10px 20px",
  },
  header: {
    fontWeight: "900 !important",
    marginBottom: "20px !important",
  },
  profileContainer: {
    // border: "1px solid black",
    width: "50%",
    display: "flex",
    justifyContent: "space-between",
    padding: "26px",
    border: "2px solid #949394 !important",
    borderRadius: "5px",
  },
  aboutContainer: {
    display: "flex",
    // alignItems: "center",
  },
  profileImg: {
    height: 120,
    width: 120,
    backgroundColor: "whitesmoke",
    borderRadius: "50%",
  },
  image: {
    height: 120,
    width: 120,
    borderRadius: "50%",
  },
  text: {
    color: "#817f7f !important",
  },
  about: {
    marginLeft: 30,
  },
  userBtn: {
    backgroundColor: "#f0eded !important",
    color: "black !important",
    marginTop: "6px !important",
  },
  editIcon: {
    margin: "0 10px !important",
    backgroundColor: "#f0eded !important",
    borderRadius: "3px !important",
  },
}));

function MyProfile() {
  const {
    root,
    header,
    profileContainer,
    aboutContainer,
    profileImg,
    image,
    text,
    about,
    userBtn,
    editIcon,
  } = useStyles();
  const navigate = useNavigate();

  return (
    <>
      <div className={root}>
        <Typography variant="h6" className={header}>
          MY PROFILE
        </Typography>
        <Box className={profileContainer}>
          <Box className={aboutContainer}>
            <Box className={profileImg}>
              <img
                src={require("../../../asset/user.png")}
                className={image}
                alt={"user"}
              ></img>
            </Box>
            <Box className={about}>
              <Typography variant="h5">Dan Schneider</Typography>
              <Typography className={text}>danschneider@gmail.com</Typography>
              <Typography className={text}>9249046704</Typography>
              <Button className={userBtn}>USER</Button>
            </Box>
          </Box>
          <Box>
            <IconButton
              className={editIcon}
              onClick={() => navigate("/changePassword")}
            >
              <LockIcon />
            </IconButton>
            <IconButton
              className={editIcon}
              onClick={() => navigate("/editProfile")}
            >
              <EditIcon />
            </IconButton>
          </Box>
        </Box>
      </div>
    </>
  );
}

export default MyProfile;
