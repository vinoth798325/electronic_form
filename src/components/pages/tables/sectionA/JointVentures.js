import React, { useContext, useEffect, useState, useMemo } from "react";
import ElectronicFormContext from "../../../../Context";
import { TableWrapper } from "../../../reusable/index";
import {
  jointVenturesColumns,
  jointVenturesCellData,
  jointVenturesContents,
  SECTIONA_JOINTVENTURES,
  ROW_ADDINGTABLE,
} from "../../../../constant/index";
import { createTableValues, updateTableValues } from "../../../../api/method";

function JointVentures() {
  const existingTableData = useContext(ElectronicFormContext);
  const [parentData, setParentData] = useState(jointVenturesCellData);

  const jointVenturesFormConfig = useMemo(() => {
    return existingTableData?.find(
      (item) => item.section === SECTIONA_JOINTVENTURES
    );
  }, [existingTableData]);

  const storeTableData = (tableData) => {
    let jsonStr = JSON.stringify(tableData);

    jointVenturesFormConfig
      ? updateTableValues(
          SECTIONA_JOINTVENTURES,
          { jointVentures: `${jsonStr}` },
          jointVenturesFormConfig.id
        )
      : createTableValues(SECTIONA_JOINTVENTURES, {
          jointVentures: `${jsonStr}`,
        });
  };
  //  fetch db data
  useEffect(() => {
    if (jointVenturesFormConfig) {
      setParentData(JSON.parse(jointVenturesFormConfig.data.jointVentures));
    }
  }, [jointVenturesFormConfig]);
  return (
    <TableWrapper
      columns={jointVenturesColumns}
      cellData={parentData}
      storeTableData={storeTableData}
      tableType={ROW_ADDINGTABLE}
      modalContent={jointVenturesContents}
    />
  );
}

export default JointVentures;
