import React, { useContext, useEffect, useState, useMemo } from "react";
import ElectronicFormContext from "../../../../Context";
import { TableWrapper } from "../../../reusable/index";
import {
  turnOverRateColumns,
  turnOverRateColumnsellData,
  turnOverRateContents,
  SECTIONA_TURNOVERRATE,
  STATIC_TABLE,
} from "../../../../constant/index";
import { createTableValues, updateTableValues } from "../../../../api/method";

function TurnOverRate() {
  const existingTableData = useContext(ElectronicFormContext);
  const [parentData, setParentData] = useState(turnOverRateColumnsellData);

  const turnoverFormConfig = useMemo(() => {
    return existingTableData?.find(
      (item) => item.section === SECTIONA_TURNOVERRATE
    );
  }, [existingTableData]);

  const storeTableData = (tableData) => {
    let jsonStr = JSON.stringify(tableData);

    turnoverFormConfig
      ? updateTableValues(
          SECTIONA_TURNOVERRATE,
          { turnOverRate: `${jsonStr}` },
          turnoverFormConfig.id
        )
      : createTableValues(SECTIONA_TURNOVERRATE, {
          turnOverRate: `${jsonStr}`,
        });
  };
  //   fetch db data
  useEffect(() => {
    if (turnoverFormConfig) {
      setParentData(JSON.parse(turnoverFormConfig.data.turnOverRate));
    }
  }, [turnoverFormConfig]);
  return (
    <TableWrapper
      columns={turnOverRateColumns}
      cellData={parentData}
      storeTableData={storeTableData}
      tableType={STATIC_TABLE}
      modalContent={turnOverRateContents}
    />
  );
}

export default TurnOverRate;
