import React, { useContext, useEffect, useState, useMemo } from "react";
import { useFormik } from "formik";
import { Typography, Box, Button } from "@mui/material";
import ElectronicFormContext from "../../../../Context";
import { CSRdetailsStyles } from "../../../../styles/index";
import { createTableValues, updateTableValues } from "../../../../api/method";
import {
  CustomTextField,
  CustomRadioButton,
  CustomizedSnackbars,
} from "../../../reusable/index";
import {
  CSRdetailsConstants,
  CSRdetailsInitialvalues,
  CSRdetailsOptions,
  CSRdetailsContent,
  SECTIONA_CSRDETAILS,
} from "../../../../constant/index";

function CSRdetails() {
  const { list, root, rootAlign, btnContainer } = CSRdetailsStyles();

  const existingTableData = useContext(ElectronicFormContext);
  const [parentData, setParentData] = useState(null);
  const [open, setOpen] = React.useState(false);

  const CSRdetailsFormConfig = useMemo(() => {
    return existingTableData?.find(
      (item) => item.section === SECTIONA_CSRDETAILS
    );
  }, [existingTableData]);

  const storeTableData = (formData) => {
    let jsonStr = JSON.stringify(formData);

    CSRdetailsFormConfig
      ? updateTableValues(
          SECTIONA_CSRDETAILS,
          { CSRdetails: `${jsonStr}` },
          CSRdetailsFormConfig.id
        )
      : createTableValues(SECTIONA_CSRDETAILS, {
          CSRdetails: `${jsonStr}`,
        });
  };

  //  fetch db data
  useEffect(() => {
    if (CSRdetailsFormConfig) {
      setParentData(JSON.parse(CSRdetailsFormConfig.data.CSRdetails));
    }
  }, [CSRdetailsFormConfig]);

  //   snackbar
  const handleClickSnackBar = () => {
    setOpen(true);
  };
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const resetData = () => {
    // setParentData(parentData);
  };

  // formik
  const onSubmit = (values) => {
    storeTableData(values);
    handleClickSnackBar();
  };

  const formik = useFormik({
    initialValues: parentData || CSRdetailsInitialvalues,
    onSubmit,
    enableReinitialize: true,
  });
  const { values, handleChange, errors, touched, handleBlur, handleSubmit } =
    formik;

  return (
    <>
      <div className={root}>
        <div className={rootAlign}>
          <Typography variant="h6">{CSRdetailsContent.title}</Typography>

          {CSRdetailsConstants.map(({ id, name, label, type, field }) => (
            <div key={id}>
              <Typography variant="div" className={list}>
                {label}
                {field === "textField" && (
                  <CustomTextField
                    type={type}
                    name={name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values[name]}
                    error={touched[name] && errors[name]}
                    helperText={touched[name] && errors[name]}
                  />
                )}
              </Typography>
              {field === "radioButton" && (
                <CustomRadioButton
                  name={name}
                  CSRdetailsOptions={CSRdetailsOptions}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values[name]}
                  error={touched[name] && errors[name]}
                  helperText={touched[name] && errors[name]}
                />
              )}
            </div>
          ))}

          <Box className={btnContainer}>
            <Button variant="outlined" color="primary" onClick={resetData}>
              Cancel
            </Button>
            <Button variant="contained" onClick={handleSubmit}>
              Save
            </Button>
            <Button variant="contained">Upload Data</Button>
          </Box>

          <CustomizedSnackbars open={open} handleClose={handleClose} />
        </div>
      </div>
    </>
  );
}

export default CSRdetails;
