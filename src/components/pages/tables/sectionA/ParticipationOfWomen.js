import React, { useContext, useEffect, useState, useMemo } from "react";
import ElectronicFormContext from "../../../../Context";
import { TableWrapper } from "../../../reusable/index";
import {
  participationOfWomenColumns,
  participationOfWomenCellData,
  participationOfWomenContents,
  SECTIONA_PARTICIPATIONOFWOMEN,
  STATIC_TABLE,
} from "../../../../constant/index";
import { createTableValues, updateTableValues } from "../../../../api/method";

function ParticipationOfWomen() {
  const existingTableData = useContext(ElectronicFormContext);
  const [parentData, setParentData] = useState(participationOfWomenCellData);

  const participationOfWomenFormConfig = useMemo(() => {
    return existingTableData?.find(
      (item) => item.section === SECTIONA_PARTICIPATIONOFWOMEN
    );
  }, [existingTableData]);

  const storeTableData = (tableData) => {
    let jsonStr = JSON.stringify(tableData);

    participationOfWomenFormConfig
      ? updateTableValues(
          SECTIONA_PARTICIPATIONOFWOMEN,
          { participationOfWomen: `${jsonStr}` },
          participationOfWomenFormConfig.id
        )
      : createTableValues(SECTIONA_PARTICIPATIONOFWOMEN, {
          participationOfWomen: `${jsonStr}`,
        });
  };
  //  fetch db data
  useEffect(() => {
    if (participationOfWomenFormConfig) {
      setParentData(
        JSON.parse(participationOfWomenFormConfig.data.participationOfWomen)
      );
    }
  }, [participationOfWomenFormConfig]);
  return (
    <TableWrapper
      columns={participationOfWomenColumns}
      cellData={parentData}
      storeTableData={storeTableData}
      tableType={STATIC_TABLE}
      modalContent={participationOfWomenContents}
    />
  );
}

export default ParticipationOfWomen;
