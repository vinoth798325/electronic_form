import React, { useContext, useEffect, useState, useMemo } from "react";
import ElectronicFormContext from "../../../../Context";
import { TableWrapper } from "../../../reusable/index";
import {
  transparencyAndDisclosureColumns,
  transparencyAndDisclosureCellData,
  transparencyAndDisclosuresContents,
  SECTIOA_TRANSPARENCYANDDISCLOSURE,
  STATIC_TABLE,
} from "../../../../constant/index";
import { createTableValues, updateTableValues } from "../../../../api/method";

function TransparencyAndDisclosures() {
  const existingTableData = useContext(ElectronicFormContext);
  const [parentData, setParentData] = useState(
    transparencyAndDisclosureCellData
  );

  const transparencyFormConfig = useMemo(() => {
    return existingTableData?.find(
      (item) => item.section === SECTIOA_TRANSPARENCYANDDISCLOSURE
    );
  }, [existingTableData]);

  const storeTableData = (tableData) => {
    let jsonStr = JSON.stringify(tableData);

    transparencyFormConfig
      ? updateTableValues(
          SECTIOA_TRANSPARENCYANDDISCLOSURE,
          { transparencyAndDisclosure: `${jsonStr}` },
          transparencyFormConfig.id
        )
      : createTableValues(SECTIOA_TRANSPARENCYANDDISCLOSURE, {
          transparencyAndDisclosure: `${jsonStr}`,
        });
  };
  //    fetch db data
  useEffect(() => {
    if (transparencyFormConfig) {
      setParentData(
        JSON.parse(transparencyFormConfig.data.transparencyAndDisclosure)
      );
    }
  }, [transparencyFormConfig]);
  return (
    <TableWrapper
      columns={transparencyAndDisclosureColumns}
      cellData={parentData}
      storeTableData={storeTableData}
      tableType={STATIC_TABLE}
      modalContent={transparencyAndDisclosuresContents}
    />
  );
}

export default TransparencyAndDisclosures;
