import React, { useContext, useEffect, useState, useMemo } from "react";
import { useFormik } from "formik";
import { Typography, Box, Button } from "@mui/material";
import ElectronicFormContext from "../../../../Context";
import {
  CustomTextField,
  CustomTextarea,
  CustomizedSnackbars,
} from "../../../reusable/index";
import {
  listedEntityData,
  listedEntityInitialValues,
  listedEntityValidationSchema,
  SECTIONA_LISTEDENTITY,
  listedEntityContent,
} from "../../../../constant/index";
import { createTableValues, updateTableValues } from "../../../../api/method";
import { listedEntityStyles } from "../../../../styles/index";

function ListedEntity() {
  const { title, list, root, rootAlign, btnContainer } = listedEntityStyles();

  const existingTableData = useContext(ElectronicFormContext);
  const [parentData, setParentData] = useState(null);
  const [open, setOpen] = React.useState(false);

  const listedEntityFormConfig = useMemo(() => {
    return existingTableData?.find(
      (item) => item.section === SECTIONA_LISTEDENTITY
    );
  }, [existingTableData]);

  const storeTableData = (formData) => {
    let jsonStr = JSON.stringify(formData);

    listedEntityFormConfig
      ? updateTableValues(
          SECTIONA_LISTEDENTITY,
          { listedEntity: `${jsonStr}` },
          listedEntityFormConfig.id
        )
      : createTableValues(SECTIONA_LISTEDENTITY, {
          listedEntity: `${jsonStr}`,
        });
  };

  // fetch db data
  useEffect(() => {
    if (listedEntityFormConfig) {
      setParentData(JSON.parse(listedEntityFormConfig.data.listedEntity));
    }
  }, [listedEntityFormConfig]);

  // snackbar
  const handleClickSnackBar = () => {
    setOpen(true);
  };
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  // formik
  const onSubmit = (values) => {
    storeTableData(values);
    handleClickSnackBar();
  };

  const formik = useFormik({
    initialValues: parentData || listedEntityInitialValues,
    validationSchema: listedEntityValidationSchema,
    onSubmit,
    enableReinitialize: true,
  });
  const { values, handleChange, errors, touched, handleBlur, handleSubmit } =
    formik;

  return (
    <div className={root}>
      <div className={rootAlign}>
        <Typography variant="h6">{listedEntityContent.header}</Typography>
        <Typography>
          I. <span className={title}>{listedEntityContent.title}</span>
        </Typography>

        {listedEntityData.map(({ label, field, type, id, name }) => (
          <div key={id}>
            <Typography className={list} variant="subtitle">
              {label}
              {field === "textField" && (
                <CustomTextField
                  type={type}
                  name={name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values[name]}
                  error={touched[name] && errors[name]}
                  helperText={touched[name] && errors[name]}
                />
              )}
            </Typography>
            {field === "textArea" && (
              <CustomTextarea
                type={type}
                name={name}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values[name]}
                error={touched[name] && errors[name]}
                helperText={touched[name] && errors[name]}
              />
            )}
          </div>
        ))}

        <Box className={btnContainer}>
          <Button variant="contained" onClick={() => handleSubmit()}>
            Save
          </Button>
          <Button variant="contained">Upload Data</Button>
        </Box>

        <CustomizedSnackbars open={open} handleClose={handleClose} />
      </div>
    </div>
  );
}

export default ListedEntity;
