import React, { useContext, useEffect, useState, useMemo } from "react";
import { createTableValues, updateTableValues } from "../../../../api/method";
import ElectronicFormContext from "../../../../Context";
import { TableWrapper } from "../../../reusable/index";
import {
  differentlyAbledColumns,
  differentlyAbledCellData,
  SECTIONA_DIFFERENTABLED,
  ROW_FULLWIDTH_TABLE,
  differentAbledContents,
} from "../../../../constant/index";

function DifferentAbled() {
  const existingTableData = useContext(ElectronicFormContext);
  const [parentData, setParentData] = useState(differentlyAbledCellData);

  const differentAbledFormConfig = useMemo(() => {
    return existingTableData?.find(
      (item) => item.section === SECTIONA_DIFFERENTABLED
    );
  }, [existingTableData]);

  const storeTableData = (tableData) => {
    let jsonStr = JSON.stringify(tableData);

    differentAbledFormConfig
      ? updateTableValues(
          SECTIONA_DIFFERENTABLED,
          { differentAbled: `${jsonStr}` },
          differentAbledFormConfig.id
        )
      : createTableValues(SECTIONA_DIFFERENTABLED, {
          differentAbled: `${jsonStr}`,
        });
  };
  //  fetch db data
  useEffect(() => {
    if (differentAbledFormConfig) {
      setParentData(JSON.parse(differentAbledFormConfig.data.differentAbled));
    }
  }, [differentAbledFormConfig]);

  return (
    <TableWrapper
      columns={differentlyAbledColumns}
      cellData={parentData}
      storeTableData={storeTableData}
      tableType={ROW_FULLWIDTH_TABLE}
      modalContent={differentAbledContents}
    />
  );
}

export default DifferentAbled;
