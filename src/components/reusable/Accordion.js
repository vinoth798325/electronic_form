import * as React from "react";
import { useNavigate } from "react-router-dom";
import { ToggleButtonGroup, ToggleButton } from "@mui/material";
import { Tooltip, Zoom, Typography, styled } from "@mui/material";
import MuiAccordion from "@mui/material/Accordion";
import MuiAccordionSummary from "@mui/material/AccordionSummary";
import MuiAccordionDetails from "@mui/material/AccordionDetails";
import ArrowForwardIosSharpIcon from "@mui/icons-material/ArrowForwardIosSharp";
import { accordionData } from "../../constant/index";
import { accordionStyles } from "../../styles/index";

const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  "&:not(:last-child)": {
    borderBottom: 0,
  },
  "&:before": {
    display: "none",
  },
}));

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary
    expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: "0.9rem" }} />}
    {...props}
  />
))(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === "dark"
      ? "rgba(255, 255, 255, .05)"
      : "rgba(0, 0, 0, .03)",
  flexDirection: "row-reverse",
  "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
    transform: "rotate(90deg)",
  },
  "& .MuiAccordionSummary-content": {
    marginLeft: theme.spacing(1),
  },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
  borderTop: "1px solid rgba(0, 0, 0, .125)",
}));

export function CustomizedAccordions() {
  const [expanded, setExpanded] = React.useState("panel");
  const [childexpanded, setnewExpanded] = React.useState("panel");

  const childhandleChange = (panel) => (event, newExpanded) => {
    if (childexpanded === panel) {
      return setnewExpanded(false);
    }
    setnewExpanded(panel);
  };
  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
    setnewExpanded(false);
  };

  const { toggleBtn, accordionContainer } = accordionStyles();
  const navigate = useNavigate();

  // toggle button
  const [view, setView] = React.useState("individualUsers");
  const handleChangeToggler = (event, nextView) => {
    setView(nextView);
  };

  const renderButtons = (accordData) => {
    return accordData.map((data, i) => (
      <Accordion
        expanded={expanded === data.key}
        onChange={handleChange(data.key)}
        key={i}
      >
        <AccordionSummary aria-controls="panel2d-content" id="panel2d-header">
          <Typography variant="subtitle1">{data.name}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <ToggleButtonGroup
            orientation="vertical"
            value={view}
            exclusive
            onChange={handleChangeToggler}
          >
            {data.buttons.map((item, index) => {
              if (item.buttons) {
                return renderSubButtons(item, index);
              }
              return (
                <Tooltip
                  title={<p>{item.tooltipContent}</p>}
                  placement="right"
                  arrow
                  TransitionComponent={Zoom}
                  key={index}
                >
                  <ToggleButton
                    value={item.value}
                    key={item.id}
                    className={toggleBtn}
                    onClick={() => navigate(item.path)}
                  >
                    {item.name}
                  </ToggleButton>
                </Tooltip>
              );
            })}
          </ToggleButtonGroup>
        </AccordionDetails>
      </Accordion>
    ));
  };

  const renderSubButtons = (subAccordion, i) => {
    return (
      <Accordion
        expanded={childexpanded === subAccordion.key}
        onClick={childhandleChange(subAccordion.key)}
        key={i}
      >
        <AccordionSummary aria-controls="panel2d-content" id="panel2d-header">
          <Typography variant="subtitle1">{subAccordion.name}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <ToggleButtonGroup
            orientation="vertical"
            value={view}
            exclusive
            onChange={handleChangeToggler}
          >
            {subAccordion.buttons.map((item, index) => {
              return (
                <Tooltip
                  title={<p>{item.tooltipContent}</p>}
                  placement="right"
                  arrow
                  TransitionComponent={Zoom}
                  key={index}
                >
                  <ToggleButton
                    value={item.value}
                    key={item.id}
                    className={toggleBtn}
                    onClick={() => navigate(item.path)}
                  >
                    {item.name}
                  </ToggleButton>
                </Tooltip>
              );
            })}
          </ToggleButtonGroup>
        </AccordionDetails>
      </Accordion>
    );
  };

  return (
    <div className={accordionContainer}>
      {/* <Accordion onClick={() => navigate("/profile")}>
        <AccordionSummary aria-controls="panel1d-content">
          <Typography className={section}>MY PROFILE</Typography>
        </AccordionSummary>
      </Accordion> */}

      {renderButtons(accordionData)}
    </div>
  );
}
