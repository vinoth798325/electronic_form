import React from "react";
import { makeStyles } from "@mui/styles";
import TextField from "@mui/material/TextField";

const useStyles = makeStyles((theme) => ({
  errorField: {
    paddingBottom: "0 !important",
  },
}));

export function CustomTextField(props) {
  const {
    type,
    name,
    onChange,
    onBlur,
    error,
    helperText,
    value,
    headerField,
  } = props;
  const { errorField } = useStyles();

  return (
    <>
      <TextField
        id="standard-basic"
        variant="standard"
        type={type}
        name={name}
        onChange={onChange}
        onBlur={onBlur}
        error={Boolean(error)}
        value={value}
        helperText={helperText}
        className={error && errorField}
        style={headerField && { width: "70px" }}
      />
    </>
  );
}
