import React from "react";
import { makeStyles } from "@mui/styles";
import TextareaAutosize from "@mui/material/TextareaAutosize";

const useStyles = makeStyles((theme) => ({
  textArea: {
    width: 400,
    marginLeft: 20,
    resize: "none",
  },
}));

export function CustomTextarea(props) {
  const { textArea } = useStyles();
  const { name, onChange, onBlur, error, value } = props;

  return (
    <>
      <TextareaAutosize
        aria-label="minimum height"
        minRows={3}
        className={textArea}
        name={name}
        onChange={onChange}
        onBlur={onBlur}
        error={error}
        value={value}
      />
    </>
  );
}
