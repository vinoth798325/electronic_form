import * as React from "react";
import Radio from "@mui/material/Radio";
import FormControl from "@mui/material/FormControl";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";

export function CustomRadioButton(props) {
  const { CSRdetailsOptions, name, onChange, onBlur, error, value } = props;

  return (
    <FormControl>
      <RadioGroup
        aria-labelledby="demo-radio-buttons-group-label"
        name="radio-buttons-group"
        onChange={onChange}
        onBlur={onBlur}
        error={error}
        value={value || ""}
      >
        {CSRdetailsOptions.map((option, i) => (
          <FormControlLabel
            value={option}
            control={<Radio />}
            label={option}
            key={i}
            name={name}
          />
        ))}
      </RadioGroup>
    </FormControl>
  );
}
