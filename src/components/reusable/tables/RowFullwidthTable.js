import React from "react";
import { useTable, usePagination } from "react-table";
import { makeStyles } from "@mui/styles";
import { Box, Paper } from "@mui/material";
import Table from "@mui/material/Table";
import { TableBody, TableCell, TableHead, TableRow } from "@mui/material";

const rowFullwidthStyles = makeStyles((theme) => ({
  boxContainer: {
    width: "100%",
    maxHeight: "398px",
    overflowX: "auto",
    border: " 1px solid rgba(224,224,224,1)",
  },

  cellFullwidth: {
    textAlign: "center !important",
  },
  tableCellFocus: {
    "&:focus-within": {
      border: " 1px solid rgb(114 113 113) ",
    },
  },
}));

export function RowFullwidthTable(props) {
  const { columns, data, setData } = props;
  const { boxContainer, cellFullwidth, tableCellFocus } = rowFullwidthStyles();

  const updateData = (rowIndex, columnId, value) => {
    setData((prevData) =>
      prevData.map((row, index) => {
        if (index === rowIndex) {
          return {
            ...prevData[rowIndex],
            [columnId]: value,
          };
        }
        return row;
      })
    );
  };

  const headerCell = (headerGroup) => {
    return headerGroup.headers.map((column) => (
      <TableCell {...column.getHeaderProps()}>
        {column.render("Header")}
      </TableCell>
    ));
  };

  const bodyCell = (row, i) => {
    return row.cells.map((cell) => {
      return (
        <TableCell
          {...cell.getCellProps()}
          className={cell.column.type === "number" ? tableCellFocus : ""}
        >
          {cell.render("Cell")}
        </TableCell>
      );
    });
  };

  const { getTableProps, getTableBodyProps, headerGroups, prepareRow, page } =
    useTable(
      {
        columns,
        data,
        updateData,
      },
      usePagination
    );
  return (
    <>
      <Box className={boxContainer}>
        <Paper sx={{ width: "100%" }} elevation={8}>
          <Table stickyHeader aria-label="sticky table" {...getTableProps()}>
            <TableHead>
              {headerGroups.map((headerGroup) => (
                <TableRow {...headerGroup.getHeaderGroupProps()}>
                  {headerCell(headerGroup)}
                </TableRow>
              ))}
            </TableHead>

            <TableBody {...getTableBodyProps()}>
              {page.map((row, i) => {
                prepareRow(row);
                return row.original.fullwidth ? (
                  <TableRow key={i}>
                    <TableCell colSpan={12} className={cellFullwidth}>
                      {row.original.values}
                    </TableCell>
                  </TableRow>
                ) : (
                  <TableRow {...row.getRowProps()} key={i}>
                    {bodyCell(row)}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Paper>
      </Box>
    </>
  );
}
