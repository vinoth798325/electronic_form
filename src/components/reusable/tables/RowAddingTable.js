import React from "react";
import { useTable, usePagination } from "react-table";
import { makeStyles } from "@mui/styles";
import AddIcon from "@mui/icons-material/Add";
import { Tooltip, Zoom } from "@mui/material";
import Table from "@mui/material/Table";
import { TableBody, TableCell, TableHead, TableRow } from "@mui/material";
import { Typography, Box, Paper, IconButton } from "@mui/material";

const rowAddingStyles = makeStyles((theme) => ({
  addBtnContainer: {
    display: "flex ",
    justifyContent: "flex-end ",
    padding: "20px",
  },
  addBtn: {
    fontSize: "35px !important",
    color: "#1976d2 ",
  },
  tableCellFocus: {
    "&:focus-within": {
      border: " 1px solid rgb(114 113 113) ",
    },
  },
}));

export function RowAddingTable(props) {
  const { columns, data, setData } = props;
  const { tableCellFocus, addBtnContainer, addBtn } = rowAddingStyles();

  const updateData = (rowIndex, columnId, value) => {
    setData((prevData) =>
      prevData.map((row, index) => {
        if (index === rowIndex) {
          return {
            ...prevData[rowIndex],
            [columnId]: value,
          };
        }
        return row;
      })
    );
  };

  const rowAdding = () => {
    let newData = {};
    let lastValue = Object.keys(data[data.length - 1]);

    lastValue.map((item) => {
      return (newData = {
        ...newData,
        id: `${parseInt(data.length) + 1}`,
        [item]: "",
      });
    });
    setData((props) => [...props, newData]);
  };

  const headerCell = (headerGroup) => {
    return headerGroup.headers.map((column) => (
      <TableCell {...column.getHeaderProps()}>
        {column.render("Header")}
      </TableCell>
    ));
  };

  const bodyCell = (row) => {
    return row.cells.map((cell) => {
      return (
        <TableCell
          {...cell.getCellProps()}
          className={cell.column.type === "number" ? tableCellFocus : ""}
        >
          {cell.render("Cell")}
        </TableCell>
      );
    });
  };

  const { getTableProps, getTableBodyProps, headerGroups, prepareRow, page } =
    useTable(
      {
        columns,
        data,
        updateData,
      },
      usePagination
    );
  return (
    <>
      <Paper sx={{ width: "100%" }} elevation={2}>
        <Table stickyHeader aria-label="sticky table" {...getTableProps()}>
          <TableHead>
            {headerGroups.map((headerGroup) => (
              <TableRow {...headerGroup.getHeaderGroupProps()}>
                {headerCell(headerGroup)}
              </TableRow>
            ))}
          </TableHead>

          <TableBody {...getTableBodyProps()}>
            {page.map((row, i) => {
              prepareRow(row);
              return (
                <TableRow {...row.getRowProps()}>{bodyCell(row)}</TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>

      <Box className={addBtnContainer}>
        <Tooltip
          title={<Typography>Add Row</Typography>}
          placement="right"
          arrow
          TransitionComponent={Zoom}
        >
          <IconButton onClick={rowAdding}>
            <AddIcon className={addBtn} />
          </IconButton>
        </Tooltip>
      </Box>
    </>
  );
}
