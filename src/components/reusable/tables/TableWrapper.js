import React, { useEffect } from "react";
import { makeStyles } from "@mui/styles";
import HelpIcon from "@mui/icons-material/Help";
import { Typography, Box, IconButton, Button } from "@mui/material";
import { CustomizedSnackbars, CustomizedDialogs } from "../index";
import { RowFullwidthTable, RowAddingTable, StaticTable } from "../index";
import {
  ROW_ADDINGTABLE,
  STATIC_TABLE,
  ROW_FULLWIDTH_TABLE,
} from "../../../constant/index";

const staticTableStyles = makeStyles((theme) => ({
  btnContainer: {
    display: "flex",
    justifyContent: "center",
    padding: "30px 50px 0 50px",
  },
  headerContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 15,
  },
  helperBtn: {
    "&:hover": {
      color: "#3ca2f3 ",
    },
  },
}));

export function TableWrapper(props) {
  const {
    columns,
    cellData,
    tableType,
    modalContent,
    storeTableData,
    renderFormFields,
  } = props;

  console.log(cellData);

  const { btnContainer, headerContainer, helperBtn } = staticTableStyles();
  const [data, setData] = React.useState(cellData);
  const [open, setOpen] = React.useState(false);
  const [openDialog, setOpenDialog] = React.useState(false);

  useEffect(() => {
    setData(cellData);
  }, [cellData]);

  // snackbar
  const handleClickSnackBar = () => {
    setOpen(true);
  };
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };
  //  dialog box
  const handleClickOpenDialog = () => {
    setOpenDialog(true);
  };
  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const saveData = () => {
    storeTableData(data);
    handleClickSnackBar();
  };

  const resetData = () => {
    setData(cellData);
  };

  const renderTable = () => {
    if (tableType === STATIC_TABLE) {
      return (
        <StaticTable
          columns={columns}
          data={data}
          setData={setData}
          modalContent={modalContent}
        />
      );
    } else if (tableType === ROW_ADDINGTABLE) {
      return (
        <RowAddingTable
          columns={columns}
          data={data}
          initialCellData={cellData}
          setData={setData}
          modalContent={modalContent}
        />
      );
    } else if (tableType === ROW_FULLWIDTH_TABLE) {
      return (
        <RowFullwidthTable
          columns={columns}
          data={data}
          setData={setData}
          modalContent={modalContent}
        />
      );
    }
  };

  return (
    <>
      <Box className={headerContainer}>
        {modalContent.map((content, i) => (
          <div key={i}>
            <Typography variant="h6">{content.aboutTable}</Typography>
            <Typography>{content.subAboutTable}</Typography>
          </div>
        ))}
        <IconButton onClick={handleClickOpenDialog}>
          <HelpIcon className={helperBtn} color="primary" />
        </IconButton>
      </Box>

      {renderTable()}
      {renderFormFields && renderFormFields()}

      <Box className={btnContainer}>
        <Button variant="outlined" color="primary" onClick={resetData}>
          Cancel
        </Button>
        <Button variant="contained" onClick={() => saveData()}>
          Save
        </Button>
        <Button variant="contained">Upload Data</Button>
      </Box>

      <CustomizedSnackbars open={open} handleClose={handleClose} />
      <CustomizedDialogs
        openDialog={openDialog}
        handleCloseDialog={handleCloseDialog}
        modalContent={modalContent}
      />
    </>
  );
}
