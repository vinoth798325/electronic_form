import * as React from "react";
import { makeStyles } from "@mui/styles";

const MIN_TEXTAREA_HEIGHT = 50;
const regex = /^[0-9\b]+$/;

const useStyles = makeStyles((theme) => ({
  textarea: {
    minHeight: `${MIN_TEXTAREA_HEIGHT}px`,
    minWidth: "10vw",
    width: "100%",
    wordWrap: "anywhere",
    outline: "none",
    resize: "none",
    border: 0,
    fontFamily: `"Roboto","Helvetica","Arial",sans-serif`,
    fontWeight: 500,
    fontSize: "1rem",
    letterSpacing: "0.01071em",
    color: "rgba(0, 0, 0, 0.87)",
  },
}));

export const EditableCell = (props) => {
  const {
    value: initialValue,
    row: { index },
    column: { id },
    updateData,
  } = props;

  const { type, maxLength } = props.column;
  const { textarea } = useStyles();

  const [value, setValue] = React.useState(initialValue);
  const textareaRef = React.useRef(null);

  const onChange = (e) => {
    if (type === "number") {
      if (e.target.value === "" || regex.test(e.target.value)) {
        setValue(e.target.value);
      }
    } else {
      setValue(e.target.value);
    }
  };

  const onBlur = () => {
    updateData(index, id, value);
  };

  React.useEffect(() => {
    setValue(initialValue);
  }, [initialValue]);

  React.useLayoutEffect(() => {
    textareaRef.current.style.height = "inherit";
    textareaRef.current.style.height = `${Math.max(
      textareaRef.current.scrollHeight,
      MIN_TEXTAREA_HEIGHT
    )}px`;
  }, [value]);

  return (
    <textarea
      value={value}
      onChange={onChange}
      onBlur={onBlur}
      ref={textareaRef}
      className={textarea}
      maxLength={maxLength}
      style={type === "number" ? { textAlign: "center" } : {}}
    />
  );
};
