import { makeStyles } from "@mui/styles";

export const accordionStyles = makeStyles((theme) => ({
  toggleBtn: {
    borderRadius: "5px !important",
    border: "1px solid #1976d2 !important",
  },

  accordionContainer: {
    width: "450px ",
  },
}));
