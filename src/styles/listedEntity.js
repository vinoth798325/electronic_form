import { makeStyles } from "@mui/styles";

export const listedEntityStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  rootAlign: {
    width: "80%",
  },

  title: {
    textDecoration: "underline",
  },
  list: {
    display: "flex",
    alignItems: "center",
  },
  btnContainer: {
    display: "flex",
    justifyContent: "center",
    padding: "30px 50px 0 50px",
  },
}));
