import { makeStyles } from "@mui/styles";

export const CSRdetailsStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  rootAlign: {
    width: "80%",
  },

  list: {
    display: "flex",
    alignItems: "center",
  },
  btnContainer: {
    display: "flex",
    justifyContent: "center",
    padding: "50px 50px 0 50px",
  },
}));
