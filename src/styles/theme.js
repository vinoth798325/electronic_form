import { createTheme } from "@mui/material/styles";

export const theme = createTheme({
  typography: {
    h6: {
      fontWeight: 900,
      marginBottom: 10,
    },
    subtitle1: {
      fontWeight: 900,
    },
    subtitle: {
      margin: "10px 0",
    },
  },

  components: {
    MuiTableCell: {
      styleOverrides: {
        root: {
          fontWeight: 900,
          background: "transparent",
          border: " 1px solid rgba(224,224,224,1) ",
          fontSize: "1rem ",
          padding: "0.5rem ",
        },
        head: {
          background: "#e7ebf0 ",
          border: " 1px solid rgb(211 208 208) ",
          textAlign: "center ",
        },
      },
    },
    MuiToggleButton: {
      styleOverrides: {
        root: {
          padding: "12px 24px",
          margin: "10px 0",
          color: "black",
          textAlign: "left",
          display: "block",

          "&:hover": {
            backgroundColor: "#3ca2f3 ",
            color: "white ",
          },
        },
      },
    },

    MuiToggleButtonGroup: {
      styleOverrides: {
        root: {
          background: "white",
          width: "100%",
        },
        // grouped: {
        //   borderRadius: "5px ",
        //   border: "1px solid #1976d2 ",

        //   "&:not(:first-of-type)": {
        //     borderTop: "1px solid #1976d2 ",
        //     borderRadius: 5,
        //   },
        //   "&:not(:last-of-type)": {
        //     borderRadius: 5,
        //   },
        // },
      },
    },
    MuiTooltip: {
      styleOverrides: {
        tooltip: {
          fontSize: 14,
        },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          marginLeft: 10,
          paddingBottom: 10,
          "& input[type=number]::-webkit-inner-spin-button": {
            "-webkit-appearance": "none",
            margin: 0,
          },
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          margin: "0 8px ",
        },
      },
    },
  },
});
